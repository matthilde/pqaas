// ProgressQuestServerEdition.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "pqse.h"

#define PQ_UpdateWeapons()   PQ_UpdateListView(&pq, "weapon", pq.weapons, 2)
#define PQ_UpdateSpells()    PQ_UpdateListView(&pq, "spells", pq.spells, 2)
#define PQ_UpdateInventory() PQ_UpdateListView(&pq, "inventory", pq.inventory, 2)
#define PQ_UpdatePlayer()    PQ_UpdateListView(&pq, "player", pq.player, 2)
#define PQ_UpdateStats()     PQ_UpdateListView(&pq, "stats", pq.stats, 2)
#define PQ_UpdateQuests()    PQ_UpdateListView(&pq, "quests", pq.quests, 1)
#define PQ_UpdatePlot()      PQ_UpdateListView(&pq, "plot", pq.plot, 1)

#define LogStatus() printf("status \"%s\"\n", status)

/*
	STUFF TO FIX/FINISH

	- Plot list
	- Plot progress
	- Quests progress
	- Logging stuff
*/

void PQ_GetSomeEpicGamerInformation(ProgressQuest* pq);
void runTheHack(PROCESS_INFORMATION* pi) {
	ProgressQuest pq = PQ_NewObject(pi);

	PQ_UpdateWeapons();
	PQ_UpdateSpells();
	PQ_UpdateInventory();
	PQ_UpdatePlayer();
	PQ_UpdateStats();
	PQ_UpdateQuests();
	PQ_UpdatePlot();

	char *status = NULL, weaponUpdate = 0;
	UINT progress = 0, xpProgress = 0, questProgress = 0, plotProgress = 0, emcumbrance = 0,
		 prevProgress		= 100,
		 prevXpProgress		= 100,
		 prevQuestProgress  = 100,
		 prevPlotProgress   = 100;

	for (;;) {
		progress = PQ_GetProgress(&pq);
		
		printf("progress %d\n", progress);

		if (prevProgress > progress) {
			xpProgress = PQ_GetXP(&pq);
			questProgress = PQ_GetQuestProgress(&pq);
			emcumbrance = PQ_GetEmcumbrance(&pq);

			printf("xp %d\nquestprogress %d\nemcumbrance %d\n", xpProgress, questProgress, emcumbrance);

			if (status) free(status);
			status = PQ_GetStatus(&pq);

			if (scmp(status, "Negotiating purchase of better equipment..."))
				weaponUpdate = 1;
			else if (weaponUpdate) {
				weaponUpdate = 0;
				PQ_UpdateWeapons();
			}
				
			LogStatus();

			if (xpProgress < prevXpProgress) {
				PQ_UpdatePlayer();
				PQ_UpdateSpells();
			}
			if (questProgress < prevQuestProgress) {
				plotProgress = PQ_GetPlotProgress(&pq);
				if (plotProgress < prevPlotProgress)
					PQ_UpdatePlot();
				PQ_UpdateQuests();
				
				prevPlotProgress = plotProgress;
			}

			PQ_UpdateInventory();

			// gee, am almost done :happitorino:
			prevXpProgress = xpProgress;
			prevQuestProgress = questProgress;
		}

		fflush(stdout);
		prevProgress = progress;

		if (WaitForSingleObject(pi->hProcess, 250) != WAIT_TIMEOUT) break;
	}

	PQ_FreeObject(&pq);
}

int _tmain(int argc, _TCHAR* argv[]) {
	srand((UINT)time(NULL));

	log(  "Progress Quest as a Service by matthilde." \
		"\n    The program is going to wait 3 seconds for you to" \
		"\n    Click OK (if an error appears)");

	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	ZeroMemory(&si, sizeof(si));
    si.cb = sizeof(si);
    ZeroMemory(&pi, sizeof(pi));

	_TCHAR boobies[] = L"pq.exe -no-tray save.pq"; // yes, I am a boobies enjoyer.

    // Start the child process. 
    if(!CreateProcess( NULL,   // No module name (use command line)
        boobies,        // Command line
        NULL,           // Process handle not inheritable
        NULL,           // Thread handle not inheritable
        FALSE,          // Set handle inheritance to FALSE
        0,              // No creation flags
        NULL,           // Use parent's environment block
        NULL,           // Use parent's starting directory 
        &si,            // Pointer to STARTUPINFO structure
        &pi )           // Pointer to PROCESS_INFORMATION structure
    ) die("CreateProcess failed");
	Sleep(3000);

	runTheHack(&pi);

    CloseHandle( pi.hProcess );
    CloseHandle( pi.hThread );

	return 0;
}