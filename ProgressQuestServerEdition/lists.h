#pragma once

typedef struct LVElement {
	char* left;
	char* right;
} LVElement;

typedef struct GUIList {
	LVElement* buffer;

	size_t used, max;
} GUIList;

GUIList GUIList_Create();
void GUIList_Free(GUIList*);
void GUIList_Append(GUIList*, LVElement*);
void GUIList_Resize(GUIList*, size_t);