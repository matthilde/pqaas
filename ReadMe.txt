
+-----------------------------+
| PROGRESS QUEST AS A SERVICE | by matthilde
+-----------------------------+

Progress Quest as a Service is a little program I made that reads from a
Progress Quest process' GUI to log it's game progress into a stream so I
can run it in a server and transmit the game progress in another application.

How?
----

Windows API magic I guess, I never touched this API because Windows is kinda
bad but I was surprised how cool it actually was to do this kind of OS black
magic.

Why?
----

Recently I wanted to play Progress Quest again and I thought "what if I could
just let it run in a server and check in from another machine the progress of
the game?". So I've set up a Windows 7 virtual machine, learned the Windows SDK
and made this project out of boredom and curiousity about Windows' API.

You know that Progress Quest is open-source right?
--------------------------------------------------

Yes, I know that it is Free and Open Source Software but I thought it would be
much more fun to make it this way.

Does it works on <version>?
---------------------------

PQAAS has been tested on Progress Quest 6.2 and 6.4. The current frontend in
https://pqaas.matthil.de runs on PQ 6.2. 6.4 does not work, it's broken and I
am not sure if I am going to fix that. Other versions has not been tested.

Isn't that cheating?
--------------------

Idk. I don't think so.

Compiling
---------

Just use the Visual Studio Solution on Visual C++. I made this program on
Visual C++ 2010 because I don't like 2012 and higher versions. Idk if
these IDEs are fully backward compatible, but it should be probably because
I started this project Visual Studio 2005 then continued on 2010, just had to
do some conversion thing.

Licensing
---------

This project is licensed under the WTFPL license.