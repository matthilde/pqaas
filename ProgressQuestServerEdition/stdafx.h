// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once


#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <tchar.h>
#include <windows.h>
#include <commctrl.h>
#include <math.h>
#include "lists.h"

// #define _NO_MSGBOX_DIE
#ifdef _NO_MSGBOX_DIE
# define die(message) { \
	log("%s (0x%08x)\n", message, GetLastError()); \
	log("Press any key to continue..."); getchar(); \
	ExitProcess(1); \
}
#else
static void die(const char* message) {
	const char* fmt = "%s (0x%08x)\n";
	char* buf; DWORD err = GetLastError();
	SIZE_T l = _scprintf(fmt, message, err);
	buf = (char*)malloc(sizeof(char) * (l+1));
	if (!buf) { buf = "No Memory"; } // I guess that if it can't be malloc'd, this is a memory problem.
	else sprintf(buf, fmt, message, err);

	MessageBoxA(NULL, buf, "Error!", MB_ICONERROR);
	free(buf);
	ExitProcess(1);
}
#endif
//// Logging stuff
#define log(...) { fprintf(stderr, "[#] "); fprintf(stderr, __VA_ARGS__); putchar('\n'); }

#define scmp(x, y) (strcmp(x, y) == 0)