#include "stdafx.h"
#include "pqse.h"
#include <string.h>


static ProgressQuest pqslave; // Ah yes, enslaved Progress Quest;
static int pq_iter;

// >uses C++
// >is actually writing C code

///////////////////////////////////// doing the black magic... ////////////////////////////////////////

static BOOL CALLBACK get_pq_window_proc(HWND hwnd, LPARAM lparam) {
	char title[256];
	if (!hwnd || !IsWindowVisible(hwnd) || !SendMessageA(hwnd, WM_GETTEXT,
											256, (LPARAM)title)) return TRUE;
	log("Found window with title: %s\n", title);

	int ti = rand() % PQ_GAMETITLES_LENGTH;
	SendMessage(hwnd, WM_SETTEXT, NULL, (LPARAM)PQ_GameTitles[ti]);
	*(HWND*)lparam = hwnd;
	return FALSE;
}

HWND get_pq_window(DWORD thread_id) {
	HWND hwnd = NULL;
	EnumThreadWindows(thread_id, get_pq_window_proc, (LPARAM)&hwnd);
	return hwnd;
}

static BOOL CALLBACK list_windows(HWND hwnd, LPARAM lparam) {
	char text[256];
	if (!hwnd || !IsWindowVisible(hwnd) || !SendMessageA(hwnd, WM_GETTEXT, 256, (LPARAM)text)) return TRUE;
	printf(" --> %s\n", text);
	return TRUE; // lon...
}

static BOOL CALLBACK get_pq_ctrl_proc(HWND hwnd, LPARAM lparam) {
	char a[256];

	GetClassNameA(hwnd, a, 256);
	if (scmp(a, "SysHeader32") || scmp(a, "TPanel") || scmp(a, "TListBox")) return TRUE;
	// puts(a);

	switch (pq_iter++) { // Feels like this method is gonna easily break
	case 0:  pqslave.progressbar = hwnd; break;
	case 1:  pqslave.status = hwnd; break;
	case 2:  pqslave.weapons = hwnd; break;
	case 3:  pqslave.emcumbrance = hwnd; break;
	case 4:  pqslave.inventory = hwnd; break;
	case 5: pqslave.quests = hwnd; break;
	case 6: pqslave.pPlot = hwnd; break;
	case 7: pqslave.plot = hwnd; break;
	case 8: pqslave.pQuests = hwnd; break;
	case 9: pqslave.spells = hwnd; break;
	case 10: pqslave.xp = hwnd; break;
	case 11: pqslave.stats = hwnd; break;
	case 12: pqslave.player = hwnd; break;
	}
	return TRUE;
}

void get_pq_gui_controls(HWND hwnd) {
	pq_iter = 0;
	EnumChildWindows(hwnd, get_pq_ctrl_proc, NULL);
}

ProgressQuest get_pq_window_full(DWORD thread_id) {
	HWND pq = get_pq_window(thread_id);
	if (!pq) die("Unable to find the Progress Quest window.");
	get_pq_gui_controls(pq);

	return pqslave;
}



//////////////////////////////////////////////////////////////////////////////

// mfw allocating in another process
void* PQ_Malloc(ProgressQuest* pq, SIZE_T size) {
	void* ptr = VirtualAllocEx(pq->hProcess, NULL, size, MEM_COMMIT, PAGE_READWRITE);
	if (!ptr) die("PQ_Malloc failed");
	return ptr;
}
void PQ_Free(ProgressQuest* pq, void* ptr) {
	VirtualFreeEx(pq->hProcess, ptr, 0, MEM_RELEASE);
}
#define PQ_AllocLVITEMA(pq)  (LVITEMA*)PQ_Malloc(pq, sizeof(LVITEMA))

static void PQ_UpdateLVITEMA(ProgressQuest* pq, LVITEMA* liptr, LVITEMA* li) {
	SIZE_T written_size;
	BOOL success = WriteProcessMemory(pq->hProcess, liptr, li, sizeof(LVITEMA), &written_size);

	if (!success) { die("PQ_UpdateLVITEMA: Writing failed"); }
	// else if (written_size != sizeof(li)) die("PQ_UpdateLVITEMA: Size does not match");
}
#define PQ_CommitLVITEMA(pq, li) PQ_UpdateLVITEMA(pq, pq->liAlloc, li)

////////// PROGRESS BAR STUFF
static UINT PQ_GetProgressBarInfo(ProgressQuest* pq, HWND progress_bar) {
	long v = SendMessageA(progress_bar, PBM_GETRANGE, FALSE, NULL);
	if (v == 0) die("PQ_GetProgressBarInfo: Something's wrong with the bar...");
	
	UINT pos = SendMessageA(progress_bar, PBM_GETPOS, NULL, NULL);
	return (pos * 100) / v;
}
UINT PQ_GetProgress(ProgressQuest* pq) { return PQ_GetProgressBarInfo(pq, pq->progressbar); }
UINT PQ_GetEmcumbrance(ProgressQuest* pq) { return PQ_GetProgressBarInfo(pq, pq->emcumbrance); }
UINT PQ_GetXP(ProgressQuest* pq) { return PQ_GetProgressBarInfo(pq, pq->xp); }
UINT PQ_GetQuestProgress(ProgressQuest* pq) { return PQ_GetProgressBarInfo(pq, pq->pQuests); }
UINT PQ_GetPlotProgress(ProgressQuest* pq)  { return PQ_GetProgressBarInfo(pq, pq->pPlot); }

// The user must free() after usage.
char* PQ_GetStatus(ProgressQuest* pq) {
	HWND sb = pq->status;

	LRESULT len = SendMessageA(sb, SB_GETTEXTLENGTH, 0, NULL);
	SIZE_T string_len = sizeof(char) * (len+1), readlen;

	char* msg = (char*)malloc(string_len);
	char* msg_alloc = (char*)PQ_Malloc(pq, string_len);
	
	SendMessageA(sb, SB_GETTEXTA, 0, (LPARAM)msg_alloc);
	
	ReadProcessMemory(pq->hProcess, msg_alloc, msg, string_len, &readlen);
	PQ_Free(pq, msg_alloc);

	return msg;
}

/////////// LISTVIEW STUFF
void PQ_GetSomeEpicGamerInformation(ProgressQuest* pq) {
	// char a[256]; GetClassNameA(pq->inventory, a, 256); puts(a);

	HWND inv = pq->inventory;
	long n = SendMessageA(inv, LVM_GETITEMCOUNT, 0, 0); // printf("%u\n", n);

	char* item_alloc = (char*)PQ_Malloc(pq, 512); char item[512]; SIZE_T readlen;
	LVITEMA* li_alloc = PQ_AllocLVITEMA(pq);
	LVITEMA li; {
		li.iItem = 0;
		li.iSubItem = 0;
		li.pszText = item_alloc;
		li.cchTextMax = 512;
	}
	PQ_UpdateLVITEMA(pq, li_alloc, &li);

	for (LRESULT i = 0; i < n; ++i) {
		li.iSubItem = 1;
		PQ_UpdateLVITEMA(pq, li_alloc, &li);

		SendMessageA(inv, LVM_GETITEMTEXTA, i, (LPARAM)li_alloc);
		ReadProcessMemory(pq->hProcess, item_alloc, item, 512, &readlen);
		fprintf(stderr, " * %sx\t", item);

		li.iSubItem = 0;
		PQ_UpdateLVITEMA(pq, li_alloc, &li);

		SendMessageA(inv, LVM_GETITEMTEXTA, i, (LPARAM)li_alloc);
		ReadProcessMemory(pq->hProcess, item_alloc, item, 512, &readlen);
		fprintf(stderr, "%s\n", item);
	}

	PQ_Free(pq, item_alloc);
	PQ_Free(pq, li_alloc);
}

// TODO: Fix this function.
void PQ_UpdateListView(ProgressQuest* pq, const char* category, HWND lv, int lvsize) {
	long n = SendMessageA(lv, LVM_GETITEMCOUNT, 0, 0);

	char* item_alloc = (char*)PQ_Malloc(pq, 1024), item[1024];
	SIZE_T readlen;

	LVITEMA *li_alloc = pq->liAlloc;
	LVITEMA li; {
		li.iItem = 0;
		li.iSubItem = 0;
		li.pszText = item_alloc;
		li.cchTextMax = 1024;
	}

	printf("%s ", category);

	for (long i = 0; i < n; ++i) {
		for (int j = 0; j < lvsize; ++j) {
			li.iSubItem = j;
			PQ_CommitLVITEMA(pq, &li);

			SendMessageA(lv, LVM_GETITEMTEXTA, i, (LPARAM)li_alloc);
			ReadProcessMemory(pq->hProcess, item_alloc, item, 1024, &readlen);

			printf("\"%s\" ", item);
		}
	}

	putchar('\n');
	PQ_Free(pq, item_alloc);
}

///////////////////////////////////////////////////////////////////////////////
ProgressQuest PQ_NewObject(PROCESS_INFORMATION* pi) {
	ProgressQuest pq = get_pq_window_full(pi->dwThreadId);
	pq.hProcess = pi->hProcess;
	pq.hThread  = pi->hThread;
	pq.liAlloc = PQ_AllocLVITEMA(&pq);

	return pq;
}
void PQ_FreeObject(ProgressQuest* pq) {
	PQ_Free(pq, pq->liAlloc);
}