#pragma once

#include <windows.h>
#include "lists.h"

typedef struct ProgressQuest {
	//////////// PROCESS INFORMATION
	HANDLE hProcess; // Host Process
	HANDLE hThread;  // Host Thread

	//////////// GUI OBJECTS
	HWND progressbar;
	HWND status; // Status Bar

	HWND weapons;
	HWND spells;
	HWND inventory;
	HWND emcumbrance; // Progress Bar

	HWND xp;		  // Progress Bar
	HWND player;
	HWND stats;

	HWND quests;
	HWND pQuests;
	HWND plot;
	HWND pPlot;

	//////////// LVITEMA Cache
	LVITEMA* liAlloc; // Allocated LVITEMA struct in Progress Quest.
} ProgressQuest;

// Lol
#define PQ_GAMETITLES_LENGTH 16
extern const wchar_t* PQ_GameTitles[PQ_GAMETITLES_LENGTH];

// Black magic
HWND get_pq_window(DWORD thread_id);
void get_pq_gui_controls(HWND hwnd);
ProgressQuest get_pq_window_full(DWORD thread_id);

// Memory Allocation inside of Progress Quest (cursed)
void* PQ_Malloc(ProgressQuest* pq);
void PQ_Free(ProgressQuest* pq, void* ptr);
ProgressQuest PQ_NewObject(PROCESS_INFORMATION* pi);
void PQ_FreeObject(ProgressQuest* pq);

//// GUI access functions
// Progress Bars
UINT PQ_GetProgress(ProgressQuest*);
UINT PQ_GetEmcumbrance(ProgressQuest* pq);
UINT PQ_GetQuestProgress(ProgressQuest* pq);
UINT PQ_GetPlotProgress(ProgressQuest* pq);
UINT PQ_GetXP(ProgressQuest* pq);

// Status
char* PQ_GetStatus(ProgressQuest*);

// Lists (inventory, stuff like that.)
void PQ_UpdateListView(ProgressQuest* pq, const char*, HWND lv, int sz);