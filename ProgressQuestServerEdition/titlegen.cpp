#include "stdafx.h"
#include "pqse.h"

const wchar_t* PQ_GameTitles[PQ_GAMETITLES_LENGTH] = {
	L"Ah yes, enslaved Progress Quest.",
	L"Progress Quest: Server Edition",
	L"PQAAS: Progress Quest As A Service!",
	L"Made by matthilde! (you probably don't know who that is)",
	L"Progress Quest but it's a bottom process",
	L"Run this on a server!",
	L"That moment when you have time to waste.",
	L"Imagine unironically not using Windows in a VM, can't relate.",
	L"Windows XP and 7 rocks, Windows 10 sucks.",
	L"Yes, I have set up a Windows VM just to make this project.",
	L"Source Code: https://git.unix.lgbt/matthilde/pqaas.",
	L"Made just because I wanna keep this game running on my server.",
	L"While making this project, I've been surprised of how decent the Windows SDK is.",
	L"My website: https://matthil.de",
	L"If you see this title, you may see another one next time.",
	L"ProgressQuest good."
};