#include "stdafx.h"

////////// PROGRESS BAR STUFF
static UINT PQ_GetProgressBarInfo(HWND progress_bar) {
	UINT pos = SendMessageA(progress_bar, PBM_GETPOS, NULL, NULL);
	return pos / 50;
}
UINT PQ_GetProgress(ProgressQuest* pq) { return PQ_GetProgressBarInfo(pq->progressbar); }
UINT PQ_GetEmcumbrance(ProgressQuest* pq) { return PQ_GetProgressBarInfo(pq->emcumbrance); }
UINT PQ_GetXP(ProgressQuest* pq) { return PQ_GetProgressBarInfo(pq->xp); }
UINT PQ_GetQuestProgress(ProgressQuest* pq) { return PQ_GetProgressBarInfo(pq->pQuests); }
UINT PQ_GetPlotProgress(ProgressQuest* pq)  { return PQ_GetProgressBarInfo(pq->pPlot); }

// The user must free() after usage.
char* PQ_GetStatus(ProgressQuest* pq) {
	HWND sb = pq->status;

	LRESULT len = SendMessageA(sb, SB_GETTEXTLENGTH, 0, NULL);
	SIZE_T string_len = sizeof(char) * (len+1), readlen;

	char* msg = (char*)malloc(string_len);
	char* msg_alloc = (char*)PQ_Malloc(pq, string_len);
	
	SendMessageA(sb, SB_GETTEXTA, 0, (LPARAM)msg_alloc);
	
	ReadProcessMemory(pq->hProcess, msg_alloc, msg, string_len, &readlen);
	PQ_Free(pq, msg_alloc);

	return msg;
}

/////////// LISTVIEW STUFF
void PQ_GetSomeEpicGamerInformation(ProgressQuest* pq) {
	// char a[256]; GetClassNameA(pq->inventory, a, 256); puts(a);

	HWND inv = pq->inventory;
	LRESULT n = SendMessageA(inv, LVM_GETITEMCOUNT, 0, 0); // printf("%u\n", n);

	char* item_alloc = (char*)PQ_Malloc(pq, 512); char item[512]; SIZE_T readlen;
	LVITEMA* li_alloc = PQ_AllocLVITEMA(pq);
	LVITEMA li; {
		li.iItem = 0;
		li.iSubItem = 0;
		li.pszText = item_alloc;
		li.cchTextMax = 512;
	}
	PQ_UpdateLVITEMA(pq, li_alloc, &li);

	for (LRESULT i = 0; i < n; ++i) {
		li.iSubItem = 1;
		PQ_UpdateLVITEMA(pq, li_alloc, &li);

		SendMessageA(inv, LVM_GETITEMTEXTA, i, (LPARAM)li_alloc);
		ReadProcessMemory(pq->hProcess, item_alloc, item, 512, &readlen);
		fprintf(stderr, " * %sx\t", item);

		li.iSubItem = 0;
		PQ_UpdateLVITEMA(pq, li_alloc, &li);

		SendMessageA(inv, LVM_GETITEMTEXTA, i, (LPARAM)li_alloc);
		ReadProcessMemory(pq->hProcess, item_alloc, item, 512, &readlen);
		fprintf(stderr, "%s\n", item);
	}

	PQ_Free(pq, item_alloc);
	PQ_Free(pq, li_alloc);
}

void PQ_UpdateInventory(ProgressQuest* pq) {
	
}