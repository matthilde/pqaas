#include "stdafx.h"

// "Couldn't you just use std::vector?"
// nah. I don't do C++.

void LVElement_Free(LVElement l) {
	if (l.left) {
		free(l.left);
		l.left = NULL;
	}
	if (l.right) {
		free(l.right);
		l.right = NULL;
	}
}

GUIList GUIList_Create() {
	GUIList gl;
	gl.buffer = (LVElement*)malloc(sizeof(LVElement) * 4);
	gl.used   = 0;
	gl.max    = 4;

	return gl;
}
void GUIList_Free(GUIList* g) {
	free(g->buffer);
	g->max = -1;
	g->used = -1;
}

static void GUIList_ZeroBuffer(GUIList* g, size_t start, size_t end) {
	for (size_t i = start; i < end; ++i) {
		g->buffer[i].left = NULL;
		g->buffer[i].right = NULL;
	}
}

void GUIList_Append(GUIList* g, LVElement *lv) {
	if (g->max == -1) die("GUIList_Append: Trying to append on unallocated list.");

	(g->used)++;
	if (g->used >= g->max) {
		g->max *= 2;
		g->buffer = (LVElement*)realloc(g->buffer, sizeof(LVElement) * g->max);

		GUIList_ZeroBuffer(g, g->max / 2, g->max);
	}
	if (lv != NULL) g->buffer[(g->used)-1] = *lv;
}

void GUIList_Resize(GUIList* g, size_t s) {
	g->used = s;
	if (s > g->max) {
		size_t oldmax = g->max;
		while (g->max < s) g->max *= 2;
		g->buffer = (LVElement*)realloc(g->buffer, sizeof(LVElement) * g->max);

		GUIList_ZeroBuffer(g, oldmax, g->max);
	} else if (s < g->max) {
		for (size_t i = (g->max)-1; i >= s; --i)
			LVElement_Free(g->buffer[i]);
	}
}